async function postPost(parent, arg, context) {
    return context.prisma.createPost({
        text: arg.text,
        like: 0,
        dislike: 0,
    })
}

async function likePost(parent, arg, context) {
    const {like: prevValueLike} = await context.prisma.post({id: arg.id});

    return context.prisma.updatePost(
        {
            data: {
                like: prevValueLike + 1
            },
            where: {
                id: arg.id
            }
        })
}

async function dislikePost(parent, arg, context) {
    const {dislike: prevValueDislike} = await context.prisma.post({id: arg.id});

    return context.prisma.updatePost({
        data: {
            dislike: prevValueDislike + 1
        },
        where: {
            id: arg.id
        }})
};

async function postReview(parent, arg, context) {
    const postExist = await context.prisma.$exists.post({
        id: arg.postId
    });

    if (!postExist) {
        throw new Error(`Post with ID ${arg.postId} does not exist`)
    }

    return context.prisma.createReview({
        text: arg.text,
        post: {connect: {id: arg.postId}},
        like: 0,
        dislike: 0,

    })
}

async function likeReview(parent, arg, context) {
    const {like: prevValueLike} = await context.prisma.review({id: arg.id});

    return context.prisma.updateReview({
        data: {
            like: prevValueLike + 1
        },
        where: {
            id: arg.id
        }})
};

async function dislikeReview(parent, arg, context) {
    const {dislike: prevValueDislike} = await context.prisma.review({id: arg.id});

    return context.prisma.updateReview({
        data: {
            dislike: prevValueDislike + 1
        },
        where: {
            id: arg.id
        }})
};

module.exports = {
    postPost,
    postReview,
    likePost,
    dislikePost,
    likeReview,
    dislikeReview
};