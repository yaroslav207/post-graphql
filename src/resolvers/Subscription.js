function newPostSubscribe(parent, arg, context, info){
    return context.prisma.$subscribe.post({
        mutation_in: ['CREATED']
    }).node();
}

const newPost = {
    subscribe: newPostSubscribe,
    resolve: payload => {
        return payload
    }
};

module.exports = {
    newPost
};