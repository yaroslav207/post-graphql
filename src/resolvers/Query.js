async function posts(parent, arg, context) {
    const where = arg.filter ? {
        title: arg.filter
    } : {};

    const postList = context.prisma.posts(
        {
            where,
            skip: arg.skip,
            first: arg.first,
            orderBy: arg.orderBy
        }
    );

    const count = context.prisma
        .postsConnection({where})
        .aggregate()
        .count();


    return {
        postList,
        count
    }
}

module.exports = {
    posts
};