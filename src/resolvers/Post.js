function reviews(parent, arg, context, info) {
    return context.prisma.post({
        id: parent.id
    }).review();
}

module.exports = {
    reviews
};