function post(parent, arg, context, info) {
    return context.prisma.review({
        id: parent.id
    }).post();
}

module.exports = {
    post
};