import React, { useState } from 'react';
import { Mutation } from 'react-apollo';
import { POST_REVIEW_MUTATION, POST_QUERY } from "../../queries";

const ReviewForm = props => {
    const { postId, toggleForm } = props;
    const [text, setText] = useState('');

    const _updateStoreAfterAddingReview = (store, newReview, postId) => {
        const orderBy = 'createdAt_DESC';
        const data = store.readQuery({
            query: POST_QUERY,
            variables: {
                orderBy
            }
        })

        const reviewedPost = data.posts.postList.find(
            item => item.id === postId
        );
        reviewedPost.reviews.push(newReview);
        store.writeQuery({ query: POST_QUERY, data })
        toggleForm(false)
    }

    return (
        <div className="form-wrapper">
            <div className="input-wrapper">
                <input
                    className="input-review"
                    autoFocus
                    onChange={e => setText(e.target.value)}
                    value={text}
                />
                <Mutation
                    mutation={POST_REVIEW_MUTATION}
                    variables={{postId, text}}
                    update={(store, {data: { postReview }}) => {
                        _updateStoreAfterAddingReview(store, postReview, postId)
                    }}
                >
                    {postMutation =>
                         <button onClick={postMutation}>Post</button>
                    }
                </Mutation>
            </div>
        </div>
    )
};

export default ReviewForm