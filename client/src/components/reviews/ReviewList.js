import React, { useState } from 'react';
import ReviewItem from './ReviewItem';
import ReviewForm from './ReviewForm';

const ReviewList = ({ postId, reviews }) => {
    const [showReviewForm, toggleForm] = useState(false);

    return(
        <div className="review-list">
            {reviews?.length > 0 && <span className="review-list-title">Reviews</span> }
            {reviews?.map( item => {
                return <ReviewItem key={item.id} {...item}/>
            })}

            {showReviewForm && <ReviewForm
                postId={postId}
                toggleForm={toggleForm}
            />}

            <button className="review-button" onClick={() => toggleForm(!showReviewForm)}>
                {showReviewForm ? 'Close review' : 'Add review'}
            </button>
        </div>
    )
};

export default ReviewList
