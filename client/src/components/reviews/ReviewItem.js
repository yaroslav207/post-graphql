import React from 'react';
import like from "../../images/like.png";
import dislike from "../../images/dislike.png";
import {Mutation} from "@apollo/react-components";
import {REVIEW_DISLIKE_MUTATION, REVIEW_LIKE_MUTATION} from "../../queries";

const ReviewItem = ({id, text, like: likeCount = 0, dislike: dislikeCount = 0}) => (
    <div className="product-item">
        <div className="title-wrapper">
            <span>{text}</span>
            <div className="reaction reaction-post">
                <Mutation
                    mutation={REVIEW_LIKE_MUTATION}
                    variables={{id}}
                >
                    {
                        likePost =>
                            <div onClick={likePost}><img src={like} className="like-icon"/> {likeCount}</div>
                    }
                </Mutation>
                <Mutation
                    mutation={REVIEW_DISLIKE_MUTATION}
                    variables={{id}}
                >
                    {
                        dislikePost =>
                            <div onClick={dislikePost}><img src={dislike} className="dislike-icon"/> {dislikeCount}
                            </div>
                    }
                </Mutation>
            </div>
        </div>
    </div>
);

export default ReviewItem