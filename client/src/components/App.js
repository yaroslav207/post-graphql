import React from 'react'

import ProductList from './post/PostList';
import PostForm from './post/PostForm';

import '../style/App.css';


function App() {
  return (
    <div className="App">
        <PostForm/>
        <ProductList/>
    </div>
  );
}

export default App;
