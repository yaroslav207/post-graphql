import React from 'react';
import {Query} from 'react-apollo';
import PostItem from './PostItem';

import {POST_QUERY, NEW_POST_SUBSCRIPTION} from '../../queries';


const PostList = props => {
    const orderBy = 'createdAt_DESC';

    const _subscribeToNewPost = subscribeToMore => {
        subscribeToMore({
            document: NEW_POST_SUBSCRIPTION,
            updateQuery: (prev, {subscriptionData}) => {

                if (!subscriptionData.data) return prev;
                const {newPost} = subscriptionData.data;

                const exists = prev.posts.postList.find(({id}) => id === newPost.id);

                if (exists) return prev;

                return {
                    ...prev, posts: {
                        postList: [newPost, ...prev.posts.postList],
                        count: prev.posts.postList.length + 1,
                        __typename: prev.posts.__typename
                    }
                }
            }
        })
    };

    return (
        <Query query={POST_QUERY} variables={{orderBy}}>
            {({loading, error, data, subscribeToMore}) => {
                if (loading) return <div>Loading...</div>;
                if (error) return <div>Fetch Error</div>;
                _subscribeToNewPost(subscribeToMore);
                const { posts: {postList} } = data;
                return (
                    <div className="post-list">
                        {postList.map((item, index) => {

                            return <PostItem key={item.id} index={postList.length - index} {...item} />
                        })}
                    </div>
                )
            }}
        </Query>
    )
};

export default PostList
