import React from 'react';
import ReviewList from '../reviews/ReviewList';
import like from '../../images/like.png'
import dislike from '../../images/dislike.png'
import {POST_LIKE_MUTATION, POST_DISLIKE_MUTATION} from "../../queries";
import {Mutation} from 'react-apollo';

const PostItem = ({id, text, index, like: likeCount = 0, dislike: dislikeCount = 0, reviews}) => {

    return (
        <div className="post-item">
            <div className="text-wrapper">
                <span className="post-text"> {text}</span>
                <span className="post-id">#{index}</span>
                <div className="reaction reaction-post">
                    <Mutation
                        mutation={POST_LIKE_MUTATION}
                        variables={{id}}
                    >
                        {
                            likePost =>
                                <div onClick={likePost}><img src={like} className="like-icon"/> {likeCount}</div>
                        }
                    </Mutation>
                    <Mutation
                        mutation={POST_DISLIKE_MUTATION}
                        variables={{id}}
                    >
                        {
                            dislikePost =>
                                <div onClick={dislikePost}><img src={dislike} className="dislike-icon"/> {dislikeCount}
                                </div>
                        }
                    </Mutation>
                </div>

            </div>
            <ReviewList postId={id} reviews={reviews}/>
        </div>
    );
};

export default PostItem