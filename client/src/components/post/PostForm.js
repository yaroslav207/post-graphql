import React, { useState } from 'react';
import { Mutation } from 'react-apollo';
import { POST_POST_MUTATION, POST_QUERY } from "../../queries";

const PostForm = props => {
    const [text, setText] = useState('');

    const _updateStoreAfterAddingPost = (store, newPost) => {
        const orderBy = "createdAt_DESC";

        const data = store.readQuery({
            query: POST_QUERY,
            variables:{
                orderBy,
            }
        });

        store.writeQuery({
            query: POST_QUERY,
            data
        });
    };

    return (
        <div className="form-wrapper">
            <div className="input-wrapper">
                <textarea cols='50' rows='8' type="text" value={text} onChange={e => setText(e.target.value)}/>
            </div>

            <Mutation
                mutation={POST_POST_MUTATION}
                variables={{ text }}

                update={(store, {data: {postPost}}) => {
                    _updateStoreAfterAddingPost(store, postPost);
                }}
            >
                { postMutation =>
                    <button className="post-button" onClick={postMutation}>Add</button>
                }
            </Mutation>
        </div>
    )
};

export default PostForm