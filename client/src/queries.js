import gql from 'graphql-tag'

export const POST_QUERY = gql`
    query productQuery($orderBy: PostOrderByInput){
        posts(orderBy: $orderBy){
            count
            postList{
                id
                text
                like
                dislike
                reviews{
                    id
                    text
                    like
                    dislike
                }
            }
        }
    }
`;

export const POST_POST_MUTATION = gql`
    mutation PostMutation($text: String!){
        postPost(text: $text){
            id
            text
            like
            dislike
            reviews{
                id
                text
            }
        }
    }
`;

export const POST_REVIEW_MUTATION = gql`
    mutation PostMutation($postId: ID!, $text: String!){
        postReview(postId: $postId, text: $text){
            id
            text
        }
    }
`;

export const NEW_POST_SUBSCRIPTION = gql`
    subscription {
        newPost{
            id
            text
            like
            dislike
            reviews{
                id
                text
                like
                dislike
            }
        }
    }
`;

export const POST_LIKE_MUTATION = gql`
mutation likePost($id: ID!){
        likePost(id: $id){
            id text like dislike
        }
    }
`;

export const POST_DISLIKE_MUTATION = gql`
    mutation dislikePost($id: ID!){
        dislikePost(id: $id){
            id text like dislike
        }
    }
`;

export const REVIEW_LIKE_MUTATION = gql`
    mutation likePost($id: ID!){
        likeReview(id: $id){
            id text like dislike
        }
    }
`;

export const REVIEW_DISLIKE_MUTATION = gql`
    mutation dislikePost($id: ID!){
        dislikeReview(id: $id){
            id text like dislike
        }
    }
`;



